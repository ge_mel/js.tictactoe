const readline = require('readline'),
			rl = readline.createInterface({
				  input: process.stdin,
				  output: process.stdout
				});

function Tictactoe(){
	this.game = [];
	this.game.marks = [ 'X' , 'O' ]; // Player X is going first
	this.game.players = [
		{ name: 'usr', mark: null, currentTurn: false, countMatches: Number, winningBlocks: [], possibleCombinations: [] },
		{ name: 'cpu', mark: null, currentTurn: false, countMatches: Number, winningBlocks: [], possibleCombinations: [] }
	];
	this.game.players.selectionState = (userName) => {
		if(userName === 'usr'){
			return this.game.boardblocks.filter(block => block.checked === true && block.player === 'usr');	
		} else if (userName === 'cpu') {
			return this.game.boardblocks.filter(block => block.checked === true && block.player === 'cpu');
		} else {
			console.log(`There is not a player by the name ${userName}`)
		}
	};
	this.game.players.updateTurnState = (user) => {
		// Update player turn state
		this.game.players.filter(player => player.name === this.game.players[parseInt(user)].name)[0].currentTurn = false;
		this.game.players.filter(player => player.name !== this.game.players[parseInt(user)].name)[0].currentTurn = true;
		// and switch turn
		this._switchTurn();
	},
	this.game.boardblocks = [
	//a.board like,
	// _|_|_
	// _|_|_
	//  | |
	/* this can be an 9 length array. ! make this dynamic (so that the game could be played in boards eg. 10x10) */
		{ block :0, checked: null, player: null },
		{ block :1, checked: null, player: null },
		{ block :2, checked: null, player: null },
		{ block :3, checked: null, player: null },
		{ block :4, checked: null, player: null },
		{ block :5, checked: null, player: null },
		{ block :6, checked: null, player: null },
		{ block :7, checked: null, player: null },
		{ block :8, checked: null, player: null }
	];
	// b.possible solutions. ! make this dynamic (so that the game could be played in boards eg. 10x10)
	this.game.winningCombinations = [
	 [0,1,2],
	 [3,4,5],
	 [6,7,8],

	 [0,3,6],
	 [1,4,7],
	 [2,5,8],

	 [0,4,8],
	 [2,4,6]
	];

	this.game.Win = {
		player: null,
		winningComb: null
	};
	this.game.updateGotAWin = (userName, winningComb) => { // Player won!
		
			this.game.Win.player = userName;
			this.game.Win.winningComb = winningComb;
	};
}


Tictactoe.prototype = {
	do: function(fnName) {
		let name = `_${fnName}`,
			args = Array.prototype.slice.call(arguments, 1);

		if(this[name]){
			this[name].apply(this, args);
		} else {
			console.log('function does not exists');
		}
	},
	
	_beginGame: function(playerIndex) {

		const self = this;

		rl.question('Select 1 for X or 2 for O ', (answer) => {

			// player mark selection
			if(answer === '1' || answer === '2'){
				self._selectMark( parseInt(playerIndex) , parseInt(answer));
			} else {
				self._beginGame(0);
			}

		});
		
		rl.on('line', function(input) {
				
			if(input.match(new RegExp(/\b\d\b/g)) && parseInt(input) < 9) {

				self._checkBlock(self.game.boardblocks, parseInt(input), 0);

			} else {

				if(input === 'exit'){
					this.close();
				} else if (parseInt(input)) {
					console.log(`please select an integer from 0 to 8`);
				} else {
					console.log(`input not an integer, ${input}`);
				}
			}
		});

	},
	_selectMark: function(playerIndex, markIndex) {

		// User player is index 0 
		this.game.players[playerIndex].mark = this.game.marks[markIndex-1];
		// Assign player player mark
		this.game.players.filter(player => this.game.players.indexOf(player) !== playerIndex )[0].mark = this.game.marks.filter(mark => this.game.marks.indexOf(mark) !== markIndex - 1 )[0];

		// Player with X mark starts first
		this.game.players.filter(player => player.mark === 'X')[0].currentTurn = true;
		// Player with O mark goes next
		this.game.players.filter(player => player.mark !== 'X')[0].currentTurn = false; 
		
		// Initiates game after user selects mark
		this._switchTurn();
		
	},
	_userPlay: function(remainingBlocks){
		
		const rBlocks = remainingBlocks.map(item => { return item.block; });

		if(!this.game.Win.player){
			rl.setPrompt(`Please select a block from remaining blocks: ${rBlocks}\n`);
			rl.prompt();
		} else {
			// Close readline
			rl.close();
		}
	
	},
	_cpuPlay: function(board){
		// select bestMove for cpu player
		if(!this.game.Win.player){

			var bestMove = parseInt(this._miniMax(board,'cpu').index);
			
			console.log(`check >> ${bestMove}`);

			this._checkBlock(board, bestMove, 1);
			
		} else {
			// Close readline
			rl.close();
		}
	},
	_switchTurn: function() {

		const nextToPlay = this.game.players.filter(player => player.currentTurn === true)[0];
		const board = this.game.boardblocks;

		console.log(`currently playing ${nextToPlay.name.toUpperCase()}\nBOARD STATUS: `,board);

		if( this._getEmptyBlocks(board).length == 0) {
					console.log('>>>>>>>>>>> game DRAW!');
					rl.close();
		} else {
		
			this.game.players.forEach( (player, index) => {
	
				if(this._terminalSolution(board, player.name)){
	
					this.game.updateGotAWin(player.name, this._terminalSolution(board, player.name).solution);
	
					console.log('>>>>>>>>>>> player', player.name.toUpperCase(), 'WON!',this._terminalSolution(board, player.name).solution);
					rl.close();
	
				} else {
	
					if(index == this.game.players.length-1) {
	
						if(nextToPlay.name === 'usr') {
							this._userPlay(this._getEmptyBlocks(board));
						} else { // if nextToPlay name = cpu
							// select bestMove
							this._cpuPlay(board);
						}
	
					}
	
				}
	
	
			});
		}


	},
	_getEmptyBlocks: function(board) {
		return board.filter(block => block.checked === null);
	},
	_checkBlock: function(board, block, user) {
		if (board.filter(item => item.block === parseInt(block) )[0].checked === null) {

			board.filter( item => item.block === parseInt(block) )[0].checked = true;
			board.filter( item => item.block === parseInt(block) )[0].player = this.game.players[parseInt(user)].name;

			this.game.players.updateTurnState(user);

		} else {
			console.log(`${block} block already selected by the other player`);
		}

	},
	
	_miniMax: function(newBoard, player) {
		/*

			According the article bellow,
			https://medium.freecodecamp.org/how-to-make-your-tic-tac-toe-game-unbeatable-by-using-the-minimax-algorithm-9d690bad4b37

			this function should:
			    1. return a value if a terminal state is found (+10, 0, -10)
			    2. go through available spots on the board
			    3. call the minimax function on each available spot (recursion)
			    4. evaluate returning values from function calls
			    5. and return the best value

			where "terminal state" means that a player is winning.

			In the end this function should provide with the "best" _checkBlock for player cpu.

		*/

		var board = newBoard;
		var countRecurse = 0;
		var emptyBlocks = this._getEmptyBlocks(board);


		/*
		* 1. return a value if a terminal state is found (+10, 0, -10)
		*/
		if (this._terminalSolution(board, 'cpu')) { // return usr wins

			return { score: 10 };
		
		} else if (this._terminalSolution(board, 'usr')) { // return cpu wins
		
			return { score: -10 };
		
		} else if (emptyBlocks.length === 0) { // return draw
		
			return { score: 0 };
		
		} else if (!this._terminalSolution(board, player)) {
			
			var moves = [];

			/*
			* 2. go through available spots on the board
			*/
			for(let [index, item] of emptyBlocks.entries()) {
				
				var move = {};
				// move.index = board.indexOf(item);
				move.index = board.indexOf(emptyBlocks[index])

				board[move.index].checked = true;
				board[move.index].player = player;

				if(player == 'usr') {
					/*
					* 3. call the minimax function on each available spot (recursion)
					*/
					var result = this._miniMax(board, 'cpu');
					move.score = result.score;
					countRecurse++;

				} else if (player == 'cpu') {
					
					/*
					* 3. call the minimax function on each available spot (recursion)
					*/
					var result = this._miniMax(board, 'usr');
					move.score = result.score;
					countRecurse++;
					
				}

				board[move.index].checked = null;
				board[move.index].player = null;
				
				// make sure that move doesn't pre-exists and push it to the moves
				if( moves.indexOf(move) < 0 ) moves.push(move);

				// on last iteration return bestMove from collected moves
				if(index == emptyBlocks.length-1){

					/*
					* 4. evaluate returning values from function calls
					*/

					var bestMove;

					if (player === 'cpu') {

						// if cpu bestMove the first MAX from the left
						bestMove = moves.map( move => { return move.score }).indexOf( Math.max(... moves.map( move => { return parseInt(move.score)  } ) ) );
					
					} else {

						// else if usr bestMove the first MIN from the left
						bestMove = moves.map( move => { return move.score }).indexOf( Math.min(... moves.map( move => { return parseInt(move.score)  } ) ) );
					
					}
										
					/*
					* 5. and return the best value
					*/
					return moves[bestMove];

				}

			}
		

		} else {
			return false;
		}

	},
	_terminalSolution: function (board_, player){
		const playerBoardSelection = this.game.players.selectionState(player).map(block => {
			return block.block;
		}), solutions = this.game.winningCombinations;
		
		let gameWON = null;

		for(let i=0; i<solutions.length; i++) {

			var count = 0;
			for(let j=0; j<solutions[i].length; j++){
				if( playerBoardSelection.join(',').match(new RegExp(solutions[i][j])) ) {
					if(count === 2) {
						gameWON = {index: i, player: player, solution: solutions[i]};
						break;
					}
					count++;
				}
			}
		}

		return gameWON;
		
	}

}



let T = new Tictactoe;
T.do('beginGame', 0);


// T.do('checkBlock', T.game.boardblocks, 0, 0);
// T.do('checkBlock', T.game.boardblocks, 6, 1);
// T.do('checkBlock', T.game.boardblocks, 8, 0);
// T.do('miniMax', T.game.boardblocks, 'cpu' );
// rl.close();

